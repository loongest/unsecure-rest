package com.codeomitted.rest.exception;

import org.springframework.security.core.AuthenticationException;

public class UnsupportedHttpContentTypeException extends AuthenticationException {

    public UnsupportedHttpContentTypeException(String msg) {
        super(msg);
    }

    public UnsupportedHttpContentTypeException(String msg, Throwable cause) {
        super(msg, cause);
    }
}


