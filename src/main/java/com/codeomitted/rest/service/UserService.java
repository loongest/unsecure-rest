package com.codeomitted.rest.service;

import com.codeomitted.rest.entity.UserEntity;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.UUID;

public interface UserService extends UserDetailsService {

    UserEntity getUser(String username);

    List<UserEntity> getAllUsers();

    UserEntity findById(UUID id) throws EntityNotFoundException;
}
