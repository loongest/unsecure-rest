package com.codeomitted.rest;

import com.codeomitted.rest.entity.RoleEntity;
import com.codeomitted.rest.entity.UserEntity;
import com.codeomitted.rest.repository.RoleRepository;
import com.codeomitted.rest.repository.UserRepository;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;

	@Bean
	CommandLineRunner run() {
		return args -> {
			RoleEntity adminRole = RoleEntity.builder().name("ROLE_ADMIN").build();
			RoleEntity userRole = RoleEntity.builder().name("ROLE_USER").build();
			roleRepository.saveAll(Arrays.asList(adminRole, userRole));

			UserEntity admin = UserEntity.builder()
					.username("admin")
					.password(bCryptPasswordEncoder.encode("password"))
					.enabled(true)
					.role(adminRole)
					.firstName("John")
					.lastName("Wick").build();

			UserEntity user = UserEntity.builder()
					.username("user")
					.password(bCryptPasswordEncoder.encode("password"))
					.enabled(true)
					.role(userRole)
					.firstName("normal")
					.lastName("user").build();

			userRepository.save(admin);
			userRepository.save(user);
		};
	}
}
