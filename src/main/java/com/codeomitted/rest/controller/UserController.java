package com.codeomitted.rest.controller;

import com.codeomitted.rest.entity.UserEntity;
import com.codeomitted.rest.repository.UserRepository;
import com.codeomitted.rest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/users")
    public List<UserEntity> getUsers(Principal principal){
        List<UserEntity> list = userService.getAllUsers();
        return list;
    }

}

